<div class="w-2/3 px-2 rounded">
	<div class="rounded border-2 border-grey-light mb-8">
		<div class="h-12 border-grey-dark">
			<p class="p-4">Profile</p>
		</div>
		<div class="h-auto border-t-2 text-grey-dark">
			<form method="POST" action="{{ route('profile.update', $user->id) }}" class="mb-4 text-center">
				{{ method_field('PUT')}}
				{{ csrf_field()}}
				<div class="inline-block relative w-1/3 mr-auto ml-auto">
					<label class="block text-grey font-bold mb-1 mt-2" for="name">Name</label>
				    <input class="bg-white appearance-none border-2 border-grey-light hover:border-blue rounded w-full py-2 px-4 text-grey-darker mr-4" id="blog-title" type="text" name="name" value="{{ old('name', $user->name) }}">
				</div>
				<div class="inline-block relative w-1/3 ">
					<label class="block text-grey font-bold mb-1 mt-2" for="email">Email</label>
				    <input class="bg-white appearance-none border-2 border-grey-light hover:border-blue rounded w-full py-2 px-4 text-grey-darker ml-4" id="blog-title" type="email" name="email" value="{{ old('email', $user->email) }}">
				</div>
				<button class="bg-blue text-white p-2 m-4 rounded">Update</button>
			</form>
		</div>
	</div>
</div>
