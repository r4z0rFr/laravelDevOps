@if ( $param == 'profile')
	@include('layouts.partials.user.content.profile')
@endif
@if ( $param == 'security')
	@include('layouts.partials.user.content.security')
@endif
@if ( $param == 'api')
	@include('layouts.partials.user.content.api')
@endif
@if ( $param == 'spy')
	@include('layouts.partials.user.content.spy')
@endif
