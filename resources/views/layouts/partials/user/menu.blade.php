<div class="w-1/3  px-4">
	<div class=" rounded border-2 border-grey-light mb-8">
		<div class="h-12 border-grey-dark">
			<p class="p-4">Settings</p>
		</div>
		<div class="h-12 border-t-2 text-grey-dark menu_tab {{$param == 'profile' ? 'border-l-4' : '' }}">
			<a href="{{ route('profile.index', ['param' => 'profile']) }}" class="link"><p class="p-4"> <i class="fa fa-edit"></i>&nbsp;&nbsp; Profile</p></a>
		</div>
		<div class="h-12 border-t text-grey-dark menu_tab {{$param == 'security' ? 'border-l-4' : ''}}">
			<a href="{{ route('profile.index', ['param' => 'security']) }}" class="link"><p class="p-4"> <i class="fa fa-lock"></i>&emsp;&nbsp; Security</p></a>
		</div>
		<div class="h-12 border-t text-grey-dark menu_tab {{$param == 'api' ? 'border-l-4' : ''}}">
			<a href="{{ route('profile.index', ['param' => 'api']) }}" class="link"><p class="p-4"><i class="fa fa-cubes"></i>&nbsp; Api</p></a>
		</div>
		@role('admin')
		<div class="h-12 border-t text-grey-dark menu_tab {{$param == 'spy' ? 'border-l-4' : ''}}">
			<a href="{{ route('profile.index', ['param' => 'spy']) }}" class="link"><p class="p-4"><i class="fa fa-user-secret"></i>&emsp; Spy</p></a>
		</div>
		@endrole
	</div>
</div>
