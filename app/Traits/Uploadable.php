<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

trait Uploadable
{

    /**
     * Get all files and push it on a collection instance
     * @param  Request $request the request collection
     * @return Collection          collection of files
     */
    public function getFiles(Request $request)
    {
        $files = collect();

        foreach ($request->file() as $file) {
            $files->push($file);
        }

        return $files->all();
    }

    /**
     * Move the files on the public path
     * @param  array $files the uploaded images
     * @return Collection collection of all paths
     */
    public function moveToPublicPath(array $files)
    {
        $paths = collect();

        foreach ($files as $file) {
            $path = $file->move(public_path('uploads'), time() . $file->getClientOriginalName());

            $paths->push($path);
        }

        return $paths->all();
    }

    /**
     * resizes all images to the specified path
     * @param  array  $paths  collection of paths to resize
     * @param  array  $sizes  array of the size
     * @return void
     */
    public function resize(array $paths, array $sizes)
    {
        foreach ($paths as $path) {
            Image::make($path)->resize($sizes[0], $sizes[1])->save($path);
        }
    }
}
