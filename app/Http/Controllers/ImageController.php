<?php

namespace App\Http\Controllers;

use App\Jobs\ResizeImage;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    use \App\Traits\Uploadable;
    /**
     * Create (upload) an image file.
     */
    public function create()
    {
        return view('image.create');
    }

    /**
     * Store an image via a job.
     *
     * @param Request $request
     */
    public function store(Request $request)
    {
        $files = $this->getFiles($request);

        $paths = $this->moveToPublicPath($files);

        $this->resize($paths, [150, 150]);

        return view('image.create');
    }
}
