<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleForm;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $roles = Role::all();

        return view('roles.index', compact('roles'));
    }

    /**
     * Store a new Role
     * @param  RoleForm $form
     * @return Redirect
     */
    public function store(RoleForm $form)
    {
        $form->persist();

        return back();
    }


    /**
     * Destroy Specified Role
     * @param  Role   $role
     * @return Redirect
     */
    public function destroy(Role $role)
    {
        $role->delete();

        return redirect()->route('roles.index');
    }


    /**
     * Return Permission where not yet into the role
     *
     * @todo  improve this with Queries Builder
     *
     * @param  Role   $role
     * @return Redirect
     */
    public function edit(Role $role)
    {
        $rolePermissions = $role->permissions()->get();
        
        $permissions = Permission::all()->whereNotIn('name', $rolePermissions->pluck('name'))->pluck('name');
        
        return view('roles.edit', compact('rolePermissions', 'permissions', 'role'));
    }
}
