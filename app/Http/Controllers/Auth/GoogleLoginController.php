<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use \App\User;
use Socialite;

class GoogleLoginController extends Controller
{
    public function redirectToProvider()
    {
    	return Socialite::driver('google')->redirect();
    }

    public function handleProviderCallback()
    {
    	$user = $this->findOrCreateNewUser(
    		Socialite::driver('google')->user()
    	);

    	auth()->login($user);

    	return redirect('/');
    }

    public function findOrCreateNewUser($googleUser)
    {
    	$user = User::firstOrNew(['email' => $googleUser->email]);

    	if ($user->exists) 	User::saveGoogleToken($user, $googleUser);

		if (!$user->exists) User::createGoogleUser($googleUser);

		return $user;
    }
}
