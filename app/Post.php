<?php

namespace App;

use App\User;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title', 'body'
    ];

    protected $casts = [
        'is_online' => 'boolean',
    ];

    /**
     * Relationship: A post belongs to a user (author).
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Scope: Get only posts that are online.
     *
     * @param [type] $query
     */
    public function scopeOnline($query)
    {
        return $query->is_online;
    }

    

    /**
     * @return mixed
     */
    public function getFillable()
    {
        return $this->fillable;
    }
}
